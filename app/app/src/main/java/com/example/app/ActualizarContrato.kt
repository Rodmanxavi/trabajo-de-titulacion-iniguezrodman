package com.example.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.app.Funciones.Funciones
import com.example.app.Objetos.Contrato
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlin.math.roundToInt

class ActualizarContrato : AppCompatActivity() {

    private val db = Firebase.firestore
    private val arrayTipoContrato = ArrayList<String>()
    private val validaciones = Funciones()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actualizar_contrato)

        val cedula: EditText = findViewById(R.id.edtCedulaActualizarContrato)
        val nombres: EditText = findViewById(R.id.edtNombresActualizarContrato)
        val apellidos: EditText = findViewById(R.id.edtApellidosActualizarContrato)
        val telefono: EditText = findViewById(R.id.edtTelefonoActualizarContrato)
        val direccion: EditText = findViewById(R.id.edtDireccionActualizarContrato)
        val tipoContrato: AutoCompleteTextView = findViewById(R.id.edtTipoContratoActualizarContrato)
        val cargo: EditText = findViewById(R.id.edtCargoActualizarContrato)
        val sueldoBase: EditText = findViewById(R.id.edtSueldoBaseActualizarContrato)
        val sucursal: EditText = findViewById(R.id.edtSucursalActualizarContrato)
        val fechaInicio: EditText = findViewById(R.id.edtFechaInicioActualizarContrato)
        val btnGuardar: Button = findViewById(R.id.btnGuardarContratoActualizarContrato)
        val btnReestablecer: Button = findViewById(R.id.btnReinicarPantallaActualizarContrato)
        val buscarCedula: ImageView = findViewById(R.id.btnBuscarCedulaActualizarContrato)

        nombres.isEnabled = false
        apellidos.isEnabled = false
        telefono.isEnabled = false
        direccion.isEnabled = false
        tipoContrato.isEnabled = false
        cargo.isEnabled = false
        sueldoBase.isEnabled = false
        sucursal.isEnabled = false
        fechaInicio.isEnabled = false

        db.collection("TipoContrato").get().addOnSuccessListener {
            for (tipoContratoDB in it.documents) {
                arrayTipoContrato.add(tipoContratoDB.id.substring(3, 5))
            }
        }

        tipoContrato.setAdapter(
            ArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,
                arrayTipoContrato
            )
        )
        tipoContrato.threshold = 1
        tipoContrato.setOnItemClickListener { adapterView, _, i, _ ->
            tipoContrato.setText(adapterView.getItemAtPosition(i).toString())
        }

        buscarCedula.setOnClickListener {
            if (cedula.text.toString().trim() != "") {
                if (validaciones.validCedula(cedula.text.toString())) {
                    db.collection("Contrato").document("Cont" + cedula.text.toString()).get()
                        .addOnSuccessListener {
                            if (it.exists()) {
                                cedula.isEnabled=false
                                buscarCedula.isEnabled=false
                                nombres.setText(it.data!!["nombres"].toString())
                                apellidos.setText(it.data!!["apellidos"].toString())
                                telefono.isEnabled = true
                                telefono.setText(it.data!!["telefono"].toString())
                                direccion.isEnabled = true
                                direccion.setText(it.data!!["direccion"].toString())
                                tipoContrato.isEnabled = true
                                tipoContrato.setText(it.data!!["tipoContrato"].toString())
                                cargo.isEnabled = true
                                cargo.setText(it.data!!["cargo"].toString())
                                sueldoBase.isEnabled = true
                                val sueldoFormato = it.data!!["sueldoBase"].toString().toDouble()
                                sueldoBase.setText(((sueldoFormato*100).roundToInt().toDouble()/100).toString())
                                sucursal.isEnabled = true
                                sucursal.setText(it.data!!["sucursal"].toString())
                                fechaInicio.isEnabled = true
                                fechaInicio.setText(it.data!!["fechaInicio"].toString())
                                btnGuardar.isEnabled = true
                            } else {
                                Toast.makeText(
                                    this,
                                    "No existe un contrato asociado a la cédula ingresada...",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                }
            }
        }

        btnGuardar.setOnClickListener {
            var stringError = "Existen errores en algunos campos:"
            var nError = 0
            if (!(nombres.text.toString().trim() == ""
                || direccion.text.toString().trim() == ""
                || tipoContrato.text.toString() == ""
                || cargo.text.toString().trim() == ""
                || sueldoBase.text.toString().trim() == ""
                || sucursal.text.toString().trim() == ""
                || fechaInicio.text.toString().trim() == "")
            ) {
                if (!validaciones.validCedula(cedula.text.toString())) {
                    stringError+="\nCédula: \n-Ingrese una cédula válida."
                    nError++
                }
                if (!validaciones.validFecha(fechaInicio.text.toString())) {
                    stringError+="\nFecha: \n-Ingrese la fecha en formato dd/mm/yyyy"
                    nError++
                }
                if (!validaciones.validTelefono(telefono.text.toString())) {
                    stringError+="\nTeléfono: \n-Ingrese un número válido."
                    nError++
                }
                if (!validaciones.validNumero(sueldoBase.text.toString())) {
                    stringError+="\nSueldo Base: \n-Ingrese un número con hasta dos decimales."
                    nError++
                } else {
                    if (sueldoBase.text.toString().toDouble()<425) {
                        stringError+="\nSueldo Base: \n-Ingrese un sueldo mayor al mínimo ($ 425.00)."
                        nError++
                    }
                }
                if (nError == 0) {
                    db.collection("TipoContrato").document("TC_" + tipoContrato.text.toString())
                        .get()
                        .addOnSuccessListener {
                            if (it.exists()) {
                                AlertDialog.Builder(this)
                                    .setTitle("AVISO")
                                    .setMessage("Confirmar Modificación de Contrato")
                                    .setPositiveButton(R.string.aceptar) { _, _ ->
                                        db.collection("Contrato")
                                            .document("Cont" + cedula.text.toString()).set(
                                                Contrato(
                                                    cedula = cedula.text.toString(),
                                                    nombres = nombres.text.toString(),
                                                    apellidos = apellidos.text.toString(),
                                                    telefono = telefono.text.toString(),
                                                    direccion = direccion.text.toString(),
                                                    tipoContrato = tipoContrato.text.toString(),
                                                    cargo = cargo.text.toString(),
                                                    sueldoBase = sueldoBase.text.toString().toDouble(),
                                                    sucursal = sucursal.text.toString(),
                                                    fechaInicio = fechaInicio.text.toString()
                                                )
                                            ).addOnSuccessListener {
                                                Toast.makeText(
                                                    this,
                                                    "Datos almacenados con éxito...",
                                                    Toast.LENGTH_LONG
                                                ).show()
                                                cedula.setText("")
                                                nombres.setText("")
                                                apellidos.setText("")
                                                telefono.setText("")
                                                direccion.setText("")
                                                tipoContrato.setText("")
                                                cargo.setText("")
                                                sueldoBase.setText("")
                                                sucursal.setText("")
                                                fechaInicio.setText("")

                                                cedula.isEnabled = true
                                                buscarCedula.isEnabled = true
                                                nombres.isEnabled = false
                                                apellidos.isEnabled = false
                                                telefono.isEnabled = false
                                                direccion.isEnabled = false
                                                tipoContrato.isEnabled = false
                                                cargo.isEnabled = false
                                                sueldoBase.isEnabled = false
                                                sucursal.isEnabled = false
                                                fechaInicio.isEnabled = false

                                            }.addOnFailureListener {
                                                Toast.makeText(
                                                    this,
                                                    "Ha ocurrido un error...",
                                                    Toast.LENGTH_LONG
                                                )
                                                    .show()
                                            }
                                    }
                                    .setNegativeButton(R.string.Cancelar) { dialog, _ ->
                                        dialog.cancel()
                                    }
                                    .setCancelable(false)
                                    .show()
                            } else {
                                Toast.makeText(
                                    this,
                                    "El Tipo de Contrato ingresado no existe, por favor, intente nuevamente...",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                } else {
                    AlertDialog.Builder(this)
                        .setTitle("AVISO")
                        .setMessage(stringError)
                        .setPositiveButton(R.string.aceptar) { dialog, _ ->
                            dialog.cancel()
                        }
                        .setCancelable(false)
                        .show()
                }
            } else {
                Toast.makeText(this, "Rellene todos los campos...", Toast.LENGTH_LONG).show()
            }
        }

        btnReestablecer.setOnClickListener {
            cedula.setText("")
            nombres.setText("")
            apellidos.setText("")
            telefono.setText("")
            direccion.setText("")
            tipoContrato.setText("")
            cargo.setText("")
            sueldoBase.setText("")
            sucursal.setText("")
            fechaInicio.setText("")

            cedula.isEnabled = true
            buscarCedula.isEnabled = true

            nombres.isEnabled = false
            apellidos.isEnabled = false
            telefono.isEnabled = false
            direccion.isEnabled = false
            tipoContrato.isEnabled = false
            cargo.isEnabled = false
            sueldoBase.isEnabled = false
            sucursal.isEnabled = false
            fechaInicio.isEnabled = false
        }

    }
}