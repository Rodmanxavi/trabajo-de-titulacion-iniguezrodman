package com.example.app.Objetos

class RolDePago(
    var cedula: String,
    var nombres: String,
    var apellidos: String,
    var tipoContrato: String,
    var cargo: String,
    var horasTrabajadas: Int,
    var horasExtra: Int,
    var bonos: Double,
    var descuentos: Double,
    var fecha: String,
    var orderByFecha: String,
    var sueldo: Double
) {
}