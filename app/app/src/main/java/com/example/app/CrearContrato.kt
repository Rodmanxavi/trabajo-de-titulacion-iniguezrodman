package com.example.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.app.Funciones.Funciones
import com.example.app.Objetos.Contrato
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class CrearContrato : AppCompatActivity() {

    private val db = Firebase.firestore
    private val arrayTipoContrato = ArrayList<String>()
    private val validaciones = Funciones()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crear_contrato)

        val cedula: EditText = findViewById(R.id.edtCedulaCrearContrato)
        val nombres: EditText = findViewById(R.id.edtNombresCrearContrato)
        val apellidos: EditText = findViewById(R.id.edtApellidosCrearContrato)
        val telefono: EditText = findViewById(R.id.edtTelefonoCrearContrato)
        val direccion: EditText = findViewById(R.id.edtDireccionCrearContrato)
        val tipoContrato: AutoCompleteTextView = findViewById(R.id.edtTipoContratoCrearContrato)
        val cargo: EditText = findViewById(R.id.edtCargoCrearContrato)
        val sueldoBase: EditText = findViewById(R.id.edtSueldoBaseCrearContrato)
        val sucursal: EditText = findViewById(R.id.edtSucursalCrearContrato)
        val fechaInicio: EditText = findViewById(R.id.edtFechaInicioCrearContrato)
        val btnGuardar: Button = findViewById(R.id.btnGuardarContratoCrearContrato)
        val btnLimpiar: Button = findViewById(R.id.btnLimpiarCamposCrearContrato)


        db.collection("TipoContrato").get().addOnSuccessListener {
            for (tipoContratoDB in it.documents) {
                arrayTipoContrato.add(tipoContratoDB.id.substring(3, 5))
            }
        }

        tipoContrato.setAdapter(
            ArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,
                arrayTipoContrato
            )
        )
        tipoContrato.threshold = 1
        tipoContrato.setOnItemClickListener { adapterView, _, i, _ ->
            tipoContrato.setText(adapterView.getItemAtPosition(i).toString())
        }

        btnGuardar.setOnClickListener {
            var stringError = "Existen errores en algunos campos:"
            var nError = 0
            if (!(cedula.text.toString().trim() == "" || nombres.text.toString().trim() == ""
                        || apellidos.text.toString().trim() == "" || telefono.text.toString()
                    .trim() == ""
                        || direccion.text.toString()
                    .trim() == "" || tipoContrato.text.toString() == ""
                        || cargo.text.toString().trim() == "" || sueldoBase.text.toString()
                    .trim() == ""
                        || sucursal.text.toString().trim() == "" || fechaInicio.text.toString()
                    .trim() == "")
            ) {

                if (!validaciones.validCedula(cedula.text.toString())) {
                    stringError += "\nCédula: \n-Ingrese una cédula válida."
                    nError++
                }
                if (!validaciones.validTexto(nombres.text.toString())) {
                    stringError += "\nNombres: \n-Ingrese solo letras o espacios."
                    nError++
                }
                if (!validaciones.validFecha(fechaInicio.text.toString())) {
                    stringError += "\nFecha: \n-Ingrese la fecha en formato dd/mm/yyyy"
                    nError++
                }
                if (!validaciones.validTexto(apellidos.text.toString())) {
                    stringError += "\nApellidos: \n-Ingrese solo letras o espacios."
                    nError++
                }
                if (!validaciones.validTelefono(telefono.text.toString())) {
                    stringError += "\nTeléfono: \n-Ingrese un número válido."
                    nError++
                }
                if (!validaciones.validNumero(sueldoBase.text.toString())) {
                    stringError += "\nSueldo Base: \n-Ingrese un número con hasta dos decimales."
                    nError++
                } else {
                    if (sueldoBase.text.toString().toDouble() < 425) {
                        stringError += "\nSueldo Base: \n-Ingrese un sueldo mayor al mínimo ($ 425.00)."
                        nError++
                    }
                }
                if (nError == 0) {
                    db.collection("Contrato").document("Cont" + cedula.text.toString()).get()
                        .addOnSuccessListener { testContrato ->
                            if (!testContrato.exists()) {
                                db.collection("TipoContrato")
                                    .document("TC_" + tipoContrato.text.toString())
                                    .get()
                                    .addOnSuccessListener {
                                        if (it.exists()) {
                                            AlertDialog.Builder(this)
                                                .setTitle("AVISO")
                                                .setMessage("Confirmar Registro de Contrato")
                                                .setPositiveButton(R.string.aceptar) { _, _ ->
                                                    db.collection("Contrato")
                                                        .document("Cont" + cedula.text.toString())
                                                        .set(
                                                            Contrato(
                                                                cedula = cedula.text.toString(),
                                                                nombres = nombres.text.toString(),
                                                                apellidos = apellidos.text.toString(),
                                                                telefono = telefono.text.toString(),
                                                                direccion = direccion.text.toString(),
                                                                tipoContrato = tipoContrato.text.toString(),
                                                                cargo = cargo.text.toString(),
                                                                sueldoBase = sueldoBase.text.toString()
                                                                    .toDouble(),
                                                                sucursal = sucursal.text.toString(),
                                                                fechaInicio = fechaInicio.text.toString()
                                                            )
                                                        ).addOnSuccessListener {
                                                            Toast.makeText(
                                                                this,
                                                                "Datos almacenados con éxito...",
                                                                Toast.LENGTH_LONG
                                                            ).show()
                                                            cedula.setText("")
                                                            nombres.setText("")
                                                            apellidos.setText("")
                                                            telefono.setText("")
                                                            direccion.setText("")
                                                            tipoContrato.setText("")
                                                            cargo.setText("")
                                                            sueldoBase.setText("")
                                                            sucursal.setText("")
                                                            fechaInicio.setText("")
                                                        }.addOnFailureListener {
                                                            Toast.makeText(
                                                                this,
                                                                "Ha ocurrido un error...",
                                                                Toast.LENGTH_LONG
                                                            )
                                                                .show()
                                                        }
                                                }
                                                .setNegativeButton(R.string.Cancelar) { dialog, _ ->
                                                    dialog.cancel()
                                                }
                                                .setCancelable(false)
                                                .show()
                                        } else {
                                            Toast.makeText(
                                                this,
                                                "El Tipo de Contrato ingresado no existe, por favor, intente nuevamente...",
                                                Toast.LENGTH_LONG
                                            ).show()
                                        }
                                    }
                            } else {
                                Toast.makeText(
                                    this,
                                    "Ya existe un contrato asociado a la cédula ingresada...",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                } else {
                    AlertDialog.Builder(this)
                        .setTitle("AVISO")
                        .setMessage(stringError)
                        .setPositiveButton(R.string.aceptar) { dialog, _ ->
                            dialog.cancel()
                        }
                        .setCancelable(false)
                        .show()
                }
            } else {
                Toast.makeText(this, "Rellene todos los campos...", Toast.LENGTH_LONG).show()
            }
        }

        btnLimpiar.setOnClickListener {
            cedula.setText("")
            nombres.setText("")
            apellidos.setText("")
            telefono.setText("")
            direccion.setText("")
            tipoContrato.setText("")
            cargo.setText("")
            sueldoBase.setText("")
            sucursal.setText("")
            fechaInicio.setText("")
        }

    }

}

