package com.example.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlin.math.roundToInt

class RolPagoDetalle : AppCompatActivity() {

    private val db = Firebase.firestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rol_pago_detalle)

        val intentOrigen = intent.extras
        val busqueda = intentOrigen!!.get("identificador").toString()

        val cedula: EditText = findViewById(R.id.edtCedulaInformacionRolPago)
        val nombres: EditText = findViewById(R.id.edtNombresInformacionRolPago)
        val apellidos: EditText = findViewById(R.id.edtApellidosInformacionRolPago)
        val tipoContrato: EditText = findViewById(R.id.edtTipoContratoInformacionRolPago)
        val cargo: EditText = findViewById(R.id.edtCargoInformacionRolPago)
        val bonos: EditText = findViewById(R.id.edtBonosInformacionRolPago)
        val descuentos: EditText = findViewById(R.id.edtDescuentosInformacionRolPago)
        val fecha: EditText = findViewById(R.id.edtFechaInformacionRolPago)
        val sueldoNeto: EditText = findViewById(R.id.edtSueldoInformacionRolPago)

        cedula.isEnabled = false
        nombres.isEnabled = false
        apellidos.isEnabled = false
        tipoContrato.isEnabled = false
        cargo.isEnabled = false
        bonos.isEnabled = false
        descuentos.isEnabled = false
        fecha.isEnabled = false
        sueldoNeto.isEnabled = false

        db.collection("RolPago").document(busqueda).get()
            .addOnSuccessListener {
                cedula.setText(it.data!!["cedula"].toString())
                nombres.setText(it.data!!["nombres"].toString())
                apellidos.setText(it.data!!["apellidos"].toString())
                tipoContrato.setText(it.data!!["tipoContrato"].toString())
                cargo.setText(it.data!!["cargo"].toString())
                val bonosFormato = it.data!!["bonos"].toString().toDouble()
                bonos.setText(((bonosFormato*100).roundToInt().toDouble()/100).toString())
                val descuentosFormato = it.data!!["descuentos"].toString().toDouble()
                descuentos.setText(((descuentosFormato*100).roundToInt().toDouble()/100).toString())
                fecha.setText(it.data!!["fecha"].toString())
                val sueldoNetoFormato = it.data!!["sueldo"].toString().toDouble()
                sueldoNeto.setText(((sueldoNetoFormato*100).roundToInt().toDouble()/100).toString())
            }

    }
}