package com.example.app.RecyclerView

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.example.app.Objetos.RolDePago
import com.example.app.R
import com.example.app.RolPagoDetalle

class RecyclerViewRolDePago(
    private val context: Context,
    private val recyclerView: RecyclerView,
    private val list: List<RolDePago>
):RecyclerView.Adapter<RecyclerViewRolDePago.myViewHolder>() {
    inner class myViewHolder(view: View): RecyclerView.ViewHolder(view), View.OnClickListener {
        val cedula: EditText
        val nombre: EditText
        val fecha: EditText
        val cargo: EditText
        init {
            cedula = view.findViewById(R.id.edtCedulaListaRolPago)
            cedula.isEnabled = false
            nombre = view.findViewById(R.id.edtNombreListaRolPago)
            nombre.isEnabled = false
            fecha = view.findViewById(R.id.edtFechaListaRolPago)
            fecha.isEnabled = false
            cargo = view.findViewById(R.id.edtCargoListaRolPago)
            cargo.isEnabled = false
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val intent = Intent(context, RolPagoDetalle::class.java)
            intent.putExtra("identificador","RP_" + cedula.text.toString()+"_"+fecha.text.toString().replace("/",""))
            context.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_rol_pago,parent,false)
        return myViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: myViewHolder, position: Int) {
        val opcion = list[position]
        holder.nombre.setText(opcion.nombres+" "+opcion.apellidos)
        holder.cargo.setText(opcion.cargo)
        holder.cedula.setText(opcion.cedula)
        holder.fecha.setText(opcion.fecha)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}