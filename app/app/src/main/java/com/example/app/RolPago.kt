package com.example.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.app.Funciones.Funciones
import com.example.app.Objetos.RolDePago
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlin.math.roundToInt

class RolPago : AppCompatActivity() {

    private val db = Firebase.firestore
    private val validaciones = Funciones()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rol_pago)

        val cedula: EditText = findViewById(R.id.edtCedulaRegistrarRolPago)
        val nombres: EditText = findViewById(R.id.edtNombresRegistrarRolPago)
        val apellidos: EditText = findViewById(R.id.edtApellidosRegistrarRolPago)
        val tipoContrato: EditText = findViewById(R.id.edtTipoContratoRegistrarRolPago)
        val cargo: EditText = findViewById(R.id.edtCargoRegistrarRolPago)
        val horasTrabajadas: EditText = findViewById(R.id.edtHorasTrabajadasRegistrarRolPago)
        val horasExtra: EditText = findViewById(R.id.edtHorasExtraRegistrarRolPago)
        val bonos: EditText = findViewById(R.id.edtBonosRegistrarRolPago)
        val descuentos: EditText = findViewById(R.id.edtDescuentosRegistrarRolPago)
        val fecha: EditText = findViewById(R.id.edtFechaRegistrarRolPago)
        val sueldoNeto: EditText = findViewById(R.id.edtSueldosAPagarRegistrarRolPago)
        val btnGuardar: Button = findViewById(R.id.btnGuardarRolPago)
        val btnCalcular: Button = findViewById(R.id.btnCalcularRegistrarRolPago)
        val btnReinicarPantalla: Button = findViewById(R.id.btnReinicarPantallaRegistrarRolPago)
        val buscarCedula: ImageView = findViewById(R.id.btnBuscarCedulaRegistrarRolPago)

        sueldoNeto.isEnabled=false

        nombres.isEnabled = false
        apellidos.isEnabled = false
        tipoContrato.isEnabled = false
        cargo.isEnabled = false
        horasTrabajadas.isEnabled = false
        horasExtra.isEnabled = false
        bonos.isEnabled = false
        descuentos.isEnabled = false
        fecha.isEnabled = false
        btnGuardar.isEnabled = false
        btnCalcular.isEnabled = false

        buscarCedula.setOnClickListener {
            if (cedula.text.toString().trim() != "") {
                if (validaciones.validCedula(cedula.text.toString())) {
                    db.collection("Contrato").document("Cont" + cedula.text.toString()).get()
                        .addOnSuccessListener {
                            if (it.exists()) {
                                cedula.isEnabled=false
                                buscarCedula.isEnabled=false
                                nombres.setText(it.data!!["nombres"].toString())
                                apellidos.setText(it.data!!["apellidos"].toString())
                                tipoContrato.setText(it.data!!["tipoContrato"].toString())
                                cargo.setText(it.data!!["cargo"].toString())
                                horasTrabajadas.isEnabled = true
                                horasExtra.isEnabled = true
                                bonos.isEnabled = true
                                descuentos.isEnabled = true
                                fecha.isEnabled = true
                                btnCalcular.isEnabled = true
                            } else {
                                Toast.makeText(
                                    this,
                                    "No existe un contrato asociado a la cédula ingresada...",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                }
            }
        }

        btnCalcular.setOnClickListener {
            var stringError = "Existen errores en algunos campos:"
            var nError = 0
            if (!(nombres.text.toString().trim() == ""
                        || horasTrabajadas.text.toString().trim() == ""
                        || horasExtra.text.toString() == ""
                        || bonos.text.toString().trim() == ""
                        || descuentos.text.toString().trim() == ""
                        || fecha.text.toString().trim() == "")
            ) {
                if (!validaciones.validEntero(horasTrabajadas.text.toString())) {
                    stringError+="\nHoras: \n-Ingrese un número entero."
                    nError++
                } else {
                    if (horasTrabajadas.text.toString().toInt() !in 0..200) {
                        stringError+="\nHoras: \n-Ingrese una cantidad de horas entre 0 y 200."
                        nError++
                    }
                }
                if (!validaciones.validEntero(horasExtra.text.toString())) {
                    stringError+="\nHoras Extra: \n-Ingrese un número entero."
                    nError++
                } else {
                    if (horasExtra.text.toString().toInt() !in 0..20) {
                        stringError+="\nHoras Extra: \n-Ingrese una cantidad de horas entre 0 y 20."
                        nError++
                    }
                }
                if (!validaciones.validNumero(bonos.text.toString())) {
                    stringError+="\nBonos: \n-Ingrese un número con hasta dos decimales."
                    nError++
                } else {
                    if (bonos.text.toString().toDouble()<0) {
                        stringError+="\nBonos: \n-Ingrese una cantidad mayor a 0."
                        nError++
                    }
                }
                if (!validaciones.validNumero(descuentos.text.toString())) {
                    stringError+="\nDescuentos: \n-Ingrese un número con hasta dos decimales."
                    nError++
                } else {
                    if (descuentos.text.toString().toDouble()<0) {
                        stringError+="\nDescuentos: \n-Ingrese una cantidad mayor a 0."
                        nError++
                    }
                }
                if (!validaciones.validFecha(fecha.text.toString())) {
                    stringError+="\nFecha: \n-Ingrese la fecha en formato dd/mm/yyyy"
                    nError++
                }
                if (nError == 0) {
                    db.collection("TipoContrato").document("TC_" + tipoContrato.text.toString())
                        .get()
                        .addOnSuccessListener {
                            db.collection("RolPago").document("RP_" + cedula.text.toString()+"_"+fecha.text.toString().replace("/","")).get()
                                .addOnSuccessListener { fechaExisteTest ->
                                    if (!fechaExisteTest.exists()) {
                                    db.collection("Contrato").document("Cont" + cedula.text.toString()).get()
                                        .addOnSuccessListener { testFechasComparacion ->
                                            if (fecha.text.toString().split("/").asReversed().joinToString("") > testFechasComparacion.data!!["fechaInicio"].toString().split("/").asReversed().joinToString("")) {
                                                var sueldoAPagar = 0.0
                                                sueldoAPagar += horasTrabajadas.text.toString().toInt()*it.data!!["pago_hora"].toString().toDouble()
                                                sueldoAPagar += horasExtra.text.toString().toInt()*it.data!!["pago_extra"].toString().toDouble()
                                                sueldoAPagar += bonos.text.toString().toDouble()
                                                sueldoAPagar -= descuentos.text.toString().toDouble()
                                                sueldoAPagar = (sueldoAPagar*100).roundToInt().toDouble()/100
                                                sueldoNeto.setText(sueldoAPagar.toString())

                                                btnGuardar.isEnabled = true
                                            } else {
                                                AlertDialog.Builder(this)
                                                    .setTitle("AVISO")
                                                    .setMessage("La fecha indicada es anterior a la contratación...")
                                                    .setPositiveButton(R.string.aceptar) { dialog, _ ->
                                                        dialog.cancel()
                                                    }
                                                    .setCancelable(false)
                                                    .show()
                                            }
                                        }
                                    } else {
                                        AlertDialog.Builder(this)
                                            .setTitle("AVISO")
                                            .setMessage("Ya existe un rol generado para la fecha indicada...")
                                            .setPositiveButton(R.string.aceptar) { dialog, _ ->
                                                dialog.cancel()
                                            }
                                            .setCancelable(false)
                                            .show()
                                    }
                                }
                        }
                } else {
                    AlertDialog.Builder(this)
                        .setTitle("AVISO")
                        .setMessage(stringError)
                        .setPositiveButton(R.string.aceptar) { dialog, _ ->
                            dialog.cancel()
                        }
                        .setCancelable(false)
                        .show()
                }
            } else {
                Toast.makeText(this, "Rellene todos los campos...", Toast.LENGTH_LONG).show()
            }
        }

        btnGuardar.setOnClickListener {
            AlertDialog.Builder(this)
                .setTitle("AVISO")
                .setMessage("Confirmar Registro de Rol de Paogs")
                .setPositiveButton(R.string.aceptar) { _, _ ->
                    val orderFecha = fecha.text.toString().split("/").asReversed().joinToString("")
                    db.collection("RolPago")
                        .document("RP_" + cedula.text.toString()+"_"+fecha.text.toString().replace("/","")).set(
                            RolDePago(
                                cedula = cedula.text.toString(),
                                nombres = nombres.text.toString(),
                                apellidos = apellidos.text.toString(),
                                tipoContrato = tipoContrato.text.toString(),
                                cargo = cargo.text.toString(),
                                horasTrabajadas = horasTrabajadas.text.toString().toInt(),
                                horasExtra = horasExtra.text.toString().toInt(),
                                bonos = bonos.text.toString().toDouble(),
                                descuentos = descuentos.text.toString().toDouble(),
                                fecha = fecha.text.toString(),
                                orderByFecha = orderFecha,
                                sueldo = sueldoNeto.text.toString().toDouble()
                            )
                        ).addOnSuccessListener {
                            Toast.makeText(
                                this,
                                "Datos almacenados con éxito...",
                                Toast.LENGTH_LONG
                            ).show()
                            cedula.setText("")
                            nombres.setText("")
                            apellidos.setText("")
                            tipoContrato.setText("")
                            cargo.setText("")
                            horasTrabajadas.setText("")
                            horasExtra.setText("")
                            bonos.setText("")
                            descuentos.setText("")
                            fecha.setText("")
                            sueldoNeto.setText("")

                            cedula.isEnabled = true
                            buscarCedula.isEnabled = true
                            horasTrabajadas.isEnabled = false
                            horasExtra.isEnabled = false
                            bonos.isEnabled = false
                            descuentos.isEnabled = false
                            fecha.isEnabled = false
                            btnGuardar.isEnabled = false

                        }.addOnFailureListener {
                            Toast.makeText(
                                this,
                                "Ha ocurrido un error...",
                                Toast.LENGTH_LONG
                            )
                                .show()
                        }
                }
                .setNegativeButton(R.string.Cancelar) { dialog, _ ->
                    dialog.cancel()
                }
                .setCancelable(false)
                .show()
        }

        btnReinicarPantalla.setOnClickListener {
            cedula.setText("")
            nombres.setText("")
            apellidos.setText("")
            tipoContrato.setText("")
            cargo.setText("")
            horasTrabajadas.setText("")
            horasExtra.setText("")
            bonos.setText("")
            descuentos.setText("")
            fecha.setText("")
            sueldoNeto.setText("")

            cedula.isEnabled = true
            buscarCedula.isEnabled = true

            sueldoNeto.isEnabled=false
            nombres.isEnabled = false
            apellidos.isEnabled = false
            tipoContrato.isEnabled = false
            cargo.isEnabled = false
            horasTrabajadas.isEnabled = false
            horasExtra.isEnabled = false
            bonos.isEnabled = false
            descuentos.isEnabled = false
            fecha.isEnabled = false
            btnGuardar.isEnabled = false
            btnCalcular.isEnabled = false
        }


    }
}