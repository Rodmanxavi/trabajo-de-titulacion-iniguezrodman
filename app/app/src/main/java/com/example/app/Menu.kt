package com.example.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Menu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val btn1: Button = findViewById(R.id.btnRegistrarContrato)
        btn1.setOnClickListener {

            val intent = Intent(this, CrearContrato:: class.java)
            startActivity(intent)
        }

        val btn2: Button = findViewById(R.id.btnModificarContrato)
        btn2.setOnClickListener {

            val intent = Intent(this, ActualizarContrato:: class.java)
            startActivity(intent)
        }

        val btn3: Button = findViewById(R.id.btnRolPagos)
        btn3.setOnClickListener {

            val intent = Intent(this, RolPago:: class.java)
            startActivity(intent)
        }

        val btn4: Button = findViewById(R.id.btnBuscarRolPagos)
        btn4.setOnClickListener {

            val intent = Intent(this, VisorRolesPago:: class.java)
            startActivity(intent)
        }

    }
}