package com.example.app.Objetos

class Contrato(
    var cedula: String,
    var nombres: String,
    var apellidos: String,
    var telefono: String,
    var direccion: String,
    var tipoContrato: String,
    var cargo: String,
    var sueldoBase: Double,
    var sucursal: String,
    var fechaInicio: String
) {

}