package com.example.app.Funciones

import java.util.regex.Pattern

class Funciones {

    fun validFecha(fecha: String): Boolean {
        val pattern = Pattern.compile("^(?:(?:31(\\/)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)\\d{2})\$|^(?:29(\\/)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))\$|^(?:0?[1-9]|1\\d|2[0-8])(\\/)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)\\d{2})\$")
        return pattern.matcher(fecha).matches()
    }

    fun validNumero(numero: String): Boolean {
        val pattern = Pattern.compile("^\\d+(\\.\\d{1,2})?\$")
        return pattern.matcher(numero).matches()
    }

    fun validEntero(numero: String): Boolean {
        val pattern = Pattern.compile("^\\d+\$")
        return pattern.matcher(numero).matches()
    }

    fun validTexto(texto: String): Boolean {
        val pattern = Pattern.compile("^([A-Za-z]+\\s*)+\$")
        return pattern.matcher(texto).matches()
    }

    fun validTelefono(numero: String): Boolean {
        val pattern = Pattern.compile("^09\\d{8}\$")
        return pattern.matcher(numero).matches()
    }

    fun validCedula(cedula: String): Boolean {
        if (cedula.length==10) {
            val lugarCed = cedula.substring(0,2).toInt()
            if (lugarCed in 1..24 || lugarCed==30) {
                val midVal = cedula[2].digitToInt()
                if (midVal in 0..5) {
                    var sum = 0
                    for (i in 0 until cedula.length-1) {
                        if (i%2==0) {
                            var value = cedula[i].digitToInt()*2
                            if (value>=10) {
                                value -= 9
                            }
                            sum += value
                        } else {
                            sum += cedula[i].digitToInt()
                        }
                    }
                    return ((10-sum%10)%10)==cedula[9].digitToInt()
                } else {
                    return false
                }
            } else {
                return false
            }
        } else {
            return false
        }
    }

}