package com.example.app

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app.Funciones.Funciones
import com.example.app.Objetos.RolDePago
import com.example.app.RecyclerView.RecyclerViewRolDePago
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class VisorRolesPago : AppCompatActivity() {

    private val db = Firebase.firestore
    private val validaciones = Funciones()

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visor_roles_pago)

        val datosListaRolPago = ArrayList<RolDePago>()

        val cedula: EditText = findViewById(R.id.edtCedulaVisorRolPago)
        val buscarCedula: ImageView = findViewById(R.id.btnBuscarCedulaVisorRolPago)
        val listaRoles: RecyclerView = findViewById(R.id.rcvListaRoles)
        val adapterDatosDetalleDocumento =
            RecyclerViewRolDePago(this, listaRoles, datosListaRolPago)

        db.collection("RolPago").orderBy("orderByFecha", Query.Direction.DESCENDING).limit(5).get()
            .addOnSuccessListener {
                for (rolPago in it) {
                    datosListaRolPago.add(
                        RolDePago(
                            cedula = rolPago.data["cedula"].toString(),
                            nombres = rolPago.data["nombres"].toString(),
                            apellidos = rolPago.data["apellidos"].toString(),
                            tipoContrato = rolPago.data["tipoContrato"].toString(),
                            cargo = rolPago.data["cargo"].toString(),
                            horasTrabajadas = rolPago.data["horasTrabajadas"].toString().toInt(),
                            horasExtra = rolPago.data["horasExtra"].toString().toInt(),
                            bonos = rolPago.data["bonos"].toString().toDouble(),
                            descuentos = rolPago.data["descuentos"].toString().toDouble(),
                            fecha = rolPago.data["fecha"].toString(),
                            orderByFecha = rolPago.data["orderByFecha"].toString(),
                            sueldo = rolPago.data["sueldo"].toString().toDouble()
                        )
                    )
                }
                listaRoles.adapter = adapterDatosDetalleDocumento
                listaRoles.itemAnimator = DefaultItemAnimator()
                listaRoles.layoutManager =
                    LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                adapterDatosDetalleDocumento.notifyDataSetChanged()
            }

        buscarCedula.setOnClickListener {
            if (cedula.text.toString().trim() != "") {
                if (validaciones.validCedula(cedula.text.toString())) {
                    db.collection("RolPago").whereEqualTo("cedula", cedula.text.toString()).orderBy("orderByFecha",Query.Direction.DESCENDING).get()
                        .addOnSuccessListener {
                            datosListaRolPago.clear()
                            for (rolPago in it) {
                                datosListaRolPago.add(
                                    RolDePago(
                                        cedula = rolPago.data["cedula"].toString(),
                                        nombres = rolPago.data["nombres"].toString(),
                                        apellidos = rolPago.data["apellidos"].toString(),
                                        tipoContrato = rolPago.data["tipoContrato"].toString(),
                                        cargo = rolPago.data["cargo"].toString(),
                                        horasTrabajadas = rolPago.data["horasTrabajadas"].toString().toInt(),
                                        horasExtra = rolPago.data["horasExtra"].toString().toInt(),
                                        bonos = rolPago.data["bonos"].toString().toDouble(),
                                        descuentos = rolPago.data["descuentos"].toString().toDouble(),
                                        fecha = rolPago.data["fecha"].toString(),
                                        orderByFecha = rolPago.data["orderByFecha"].toString(),
                                        sueldo = rolPago.data["sueldo"].toString().toDouble()
                                    )
                                )
                            }
                            listaRoles.adapter = adapterDatosDetalleDocumento
                            listaRoles.itemAnimator = DefaultItemAnimator()
                            listaRoles.layoutManager =
                                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                            adapterDatosDetalleDocumento.notifyDataSetChanged()
                        }
                } else {
                    Toast.makeText(
                        this,
                        "La cédula ingresada no existe, por favor, intente nuevamente...",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                Toast.makeText(this, "Rellene todos los campos...", Toast.LENGTH_LONG).show()
            }
        }

    }
}